package com.kirdow.spp.render;

public class Bitmap {

    public final int width, height;
    public final int[] pixels;

    public Bitmap(int width, int height) {
        this.width = width;
        this.height = height;
        this.pixels = new int[width*height];
    }

    public void clear(int color) {
        for (int i = 0; i < pixels.length; i++)
            pixels[i] = color;
    }

}
