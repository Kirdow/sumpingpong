package com.kirdow.spp;

import com.kirdow.spp.render.Screen;

import java.awt.*;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.io.BufferedReader;

public class SumPingPong extends Canvas {

    // Core data
    public final int width, height;
    public final String gameTitle;

    // Main thread / stop thread vars
    private Thread gameThread, joinThread;
    private boolean running;

    // Screen vars
    private BufferedImage framebuffer;
    private Screen screen;

    // Game reference
    private Game game;

    /**
     * Main constructor for the main game class
     * @param width Width of the window
     * @param height Height of the window
     */
    public SumPingPong(int width, int height) {
        // Init the core data
        this.width = width;
        this.height = height;
        this.gameTitle = "SumPingPong";

        // Init useful data
        this.initThreading();
        this.initBuffer();

        // Init canvas size
        Dimension dim = new Dimension(width, height);
        this.setPreferredSize(dim);
        this.setMinimumSize(dim);
        this.setMaximumSize(dim);
        this.setSize(dim);
    }

    /**
     * Initializes the threading classes and variables for the game
     */
    private void initThreading() {
        System.out.println("Initializing threads");
        gameThread = new Thread(this::runGameLoop, this.gameTitle);
        joinThread = new Thread(this::joinThread, this.gameTitle + "-JOIN");
        running = false;
    }

    /**
     * Initializes the framebuffer
     */
    private void initBuffer() {
        System.out.println("Initializing buffer");
        framebuffer = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        screen = new Screen(framebuffer);
    }

    /**
     * Initialize method for game content
     */
    private void initGame() {
        game = new Game(this);
        game.setGraphics(screen);
    }

    /**
     * Entry point for the main game thread
     */
    private void runGameLoop() {
        System.out.println("Initializing game");
        initGame();

        System.out.println("Entering game loop");
        while (running) {
            tick();
            draw();
        }
        System.out.println("Exiting game loop");
    }

    /**
     * Tick method for the game
     */
    private void tick() {
        game.tick();
    }

    /**
     * Draw method for the game
     */
    private void draw() {
        BufferStrategy bs = this.getBufferStrategy();
        if (bs == null) {
            this.createBufferStrategy(3);
            this.requestFocus();
            return;
        }

        // Clear screen
        screen.clear(0xFF000000);

        game.draw();

        // Flush screen
        screen.flush();

        Graphics gfx = bs.getDrawGraphics();
        gfx.drawImage(framebuffer, 0, 0, width, height, null);
        gfx.dispose();
        bs.show();
    }

    /**
     * Start the game thread
     */
    public void start() {
        if (running) return;
        running = true;
        System.out.println("Starting game thread");
        gameThread.start();
    }

    /**
     * Stop the game thread
     */
    public void stop() {
        if (!running) return;
        running = false;
        joinThread.start();
    }

    /**
     * Join thread entry point
     */
    private void joinThread() {
        System.out.println("Joining game thread");
        try {
            gameThread.join();

            System.out.println("Successfully joined thread!");
        } catch (InterruptedException e) {
            System.out.println("Failed to join thread");
        }

        System.out.println("Exiting join thread");
    }

    public void join() {
        try {
            gameThread.join();
        } catch (InterruptedException e) {
        }
        try {
            joinThread.join();
        } catch (InterruptedException e) {
        }
    }

    /**
     * Used to start the game and wait till it ends
     */
    public void startAndWait() {
        System.out.println("Entry thread start!");
        start();

        System.out.println("Entry thread on hold...");
        join();
        System.out.println("Entry thread continued!");
    }



}
