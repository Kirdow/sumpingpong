package com.kirdow.spp;

import com.kirdow.spp.render.Bitmap;

public class Game {

    private final SumPingPong spp;
    private Bitmap graphics;

    private double x, y;

    public Game(SumPingPong game) {
        this.spp = game;

        x = 0.0;
        y = 0.0;
    }

    public void setGraphics(Bitmap bitmap) {
        this.graphics = bitmap;
    }

    public void tick() {
        x += 1.0 / 10.0;
        y += 1.0 / 20.0;
    }

    public void draw() {
        if (graphics == null)
            return;

        int offsetX = (int)this.x;
        int offsetY = (int)this.y;

        int tileSize = 42;

        for (int y = 0; y < graphics.height; y++) {
            int tileY = (y - offsetY);
            tileY = ((tileY < 0) ? 1 : 0) + (tileY / tileSize);

            for (int x = 0; x < graphics.width; x++) {
                int tileX = (x - offsetX);
                tileX = ((tileX < 0) ? 1 : 0) + (tileX / tileSize);

                int tileId = (tileX + tileY) % 2;

                int color = tileId == 0 ? 0xFF000000 : 0xFFFFFFFF;

                graphics.pixels[x + y * graphics.width] = color;
            }
        }
    }

}
