package com.kirdow.spp;

import javax.swing.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

public class Main {

    private static final WindowListener LISTENER;

    private static SumPingPong game = null;

    public static void main(String[] args) {
        System.out.println("Creating game object.");
        game = new SumPingPong(800, 600);

        System.out.println("Initializing window");
        JFrame frame = new JFrame(game.gameTitle);
        frame.add(game);
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.setResizable(false);
        frame.setVisible(true);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.addWindowListener(LISTENER);

        System.out.println("Game starting!");
        game.startAndWait();

        System.out.println("Game ended!");
        System.exit(0);
    }

    static {
        WindowListener listener = new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {

            }

            @Override
            public void windowClosing(WindowEvent e) {
                System.out.println("Window close button pressed");
                game.stop();
                game.join();
                e.getWindow().dispose();
            }

            @Override
            public void windowClosed(WindowEvent e) {
            }

            @Override
            public void windowIconified(WindowEvent e) {

            }

            @Override
            public void windowDeiconified(WindowEvent e) {

            }

            @Override
            public void windowActivated(WindowEvent e) {

            }

            @Override
            public void windowDeactivated(WindowEvent e) {

            }
        };

        LISTENER = listener;
    }

}
