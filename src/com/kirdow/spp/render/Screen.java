package com.kirdow.spp.render;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;

public class Screen extends Bitmap {

    private BufferedImage bufferImage;
    private int[] buffer;

    public Screen(BufferedImage image) {
        super(image.getWidth(), image.getHeight());

        this.bufferImage = image;
        this.buffer = ((DataBufferInt)image.getRaster().getDataBuffer()).getData();
    }

    public void flush() {
        for (int i = 0; i < width*height; i++)
            buffer[i] = this.pixels[i];
    }
}
